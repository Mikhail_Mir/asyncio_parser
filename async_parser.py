import asyncio
import logging
import requests
import json

# Формируем логгер
format_message = '%(asctime)s - %(levelname)s - %(message)s'
time_format = '%H:%M:%S'
logging.basicConfig(format=format_message, level=0, datefmt=time_format)
logging.StreamHandler()

answers = {}
errors = []
missing = []


async def get_text(url) -> None:
    """
    Функция отправляет запросы по указанному url, обрабатывает из и сохраняет в глобальные переменные
    :param url: адрес для получения от него ответа
    :return:
    """
    await asyncio.sleep(0)
    """Ессли адрес не в черном списке, то успешно полученный ответ, созраняет в словарь answers"""
    global loop_start
    logging.info('Готовимся к запросу на адрес {}'.format(url))
    if url in black_list:
        logging.warning('Внимание адрес {} в черном списке и будет пропущен'.format(url))
        missing.append(url)
        return
    try:
        r = get_response(url)
        if r.status_code == 200:
            logging.info('Ответ от {} получен ------------------ 200'.format(url))
            time = round(loop.time() - loop_start, 3)

            loop_start = loop.time()
            data = {
                'code': r.status_code,
                'time': time,
                'content': r.text
            }
            answers.update({url: data})
    except BaseException:
        # В случае исключений, адрес вызвавший ошибку дозаписывается в список ошибок
        logging.error('Ответ неверный от {}'.format(url))
        errors.append(url)


def get_response(url) -> requests:
    """
    Запрос по переданному адресу
    :param url: адрес для запроса
    :return: response от url
    """
    logging.info('Получаем ответ от {}'.format(url))
    return requests.get(url=url)


def save_in_file(file_name: str, data, format_file: str = 'json'):
    """
    Записывает полученную информацию в файл в указанном формате.
    Если формат файла - json, сериализует данные.
    :param file_name: строковое имя файлы
    :param data: полученные данные для сохранения в файле
    :param format_file: формат файла
    :return:
    """
    file = file_name + '.' + format_file
    if format_file == 'json':
        data = json.dumps(data)
    with open(file, 'w') as some_file:
        some_file.write(data)


def read_from_file(file_name: str, format_file: str = 'json'):
    """
    Функция чтения данных из файла
    :param file_name: строковое имя файла
    :param format_file: строковый формат файла
    :return: Данные из файла. Если это json - десириализует данные.
    """
    file = file_name + '.' + format_file
    with open(file, 'r') as f:
        logging.info('Получение списка всех url-ов из файла ...')
        data = f.read()
    if format_file == 'json':
        data = json.loads(data)
    return data


try:
    black_list = read_from_file('black_list')
except FileNotFoundError:
    black_list = []

# Создаем event loop
loop = asyncio.get_event_loop()
# Точки старта работы event loop
start = loop_start = loop.time()

logging.info('Получение списка всех url-ов из файла ...')
# Срезами можно регулировать кол-во адресов с которыми будет работать
all_urls = read_from_file('urls_list')[:200]

logging.info('Адреса получены в кол-ве: {}'.format(len(all_urls)))

# Формирование "тасок" и логики их ожидания
tasks = [loop.create_task(get_text(url)) for url in all_urls]
wait_tasks = asyncio.wait(tasks)
loop.run_until_complete(wait_tasks)
total_time = loop.time() - start
loop.close()

# Дозаписываем адреса с ошибками в файл с черным списком.
if errors:
    black_list.extend(errors)
    black_list = set(black_list)
    save_in_file(file_name='black_list', data=list(black_list))

logging.info('Общее время работы скрипта' + str(round(total_time, 3)) + 'sec')

# Созраняем результаты парсинга
save_in_file(file_name='parse_result', data=answers)
# Вывод краткой итоговой информации по кол-ву елементов в глобальных переменных (статусы)
print(f'Ошибок случилось - {len(errors)}/{len(all_urls)}, пропущено - {len(missing)} адресов.')

import json
import requests
from bs4 import BeautifulSoup as bs
import logging


format_message = '%(asctime)s - %(levelname)s - %(message)s'
time_format = '%H:%M:%S'
logging.basicConfig(format=format_message, level=0, datefmt=time_format)
logging.StreamHandler()


def get_all_urls(url='http://edu-top.ru/katalog/urls.php'):
    logging.info('Парсинг списка всех url-ов из {}'.format(url))
    urls = requests.get(url)
    soup = bs(urls.content, 'html.parser')
    return [url['href'] for url in soup.findAll('a')]


if __name__ == '__main__':
    urls_list = get_all_urls()
    with open('urls_list.json', 'w') as base:
        base.write(json.dumps(urls_list))
        logging.info('Адреса сохранены в файл. {} адресов'.format(len(urls_list)))
